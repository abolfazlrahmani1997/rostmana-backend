@extends('Website.layout.app')
@section('css')


@endsection
@section('content')
    <div class="container-fluid d-flex img-fluid" id="back-about">
    <h1 class="mt-5 mr-5">درباره ما </h1>
    </div>

    <div class="container col-lg-12 col-md-12 col-sm-11  my-5 mx-auto text-center" style="">
        <div class="row col-lg-12 col-md-12">
            <div class="col-lg-5 col-sm-12 bg-light rounded-right mr-0 " id="back-aboutME">
            </div>
            <div class="col-lg-7 col-sm-12 rounded-left  justify-content-center  py-5 py-sm-0   px-5" style="font-size:14px;background-color: #dedede;text-align: center">
                <b>استارتاپی بر پایه اینترنت اشیاء </b>
                <br>
                <p>آغاز فعالیت تیم رٌست مانا بر پایه ایده‌ای هوشمندانه در سه خدمت طراحی گلخانه‌هایی دکوراتیو در ابعاد محدود، طراحی فضاهای سبز داخلی، فروش آنلاین گیاهان با قابلیت کنترل از راه دور توسط اپلیکیشن  در دی ماه سال ۱۳۹۸ شکل گرفت. هدف اصلی این مجموعه فراهم آوردن رویشی ماندگار با استفاده از سیستم اتوماسیون هوشمند برای گیاهان با شرایط نگه‌داری متفاوت و اقلیم‌های خاص می‌باشد. در این زمینه نیروهای متخصص با استفاده از فناوری‌های نوین، پیوسته در تلاش می‌باشند تا محصولی با کیفیت و کارآمد را متناسب با سلیقه مصرف‌کننده عرضه نمایند. لازم به ذکر است که این مجموعه آماده به همکاری با گروه‌های طراحی و معماری می‌باشد و با تلاشی روز افزون در راه پیاده‌سازی ایده‌های جدید و متفاوت گام برمی‌دارد. در حال حاضر رست‌مانا نمونه اولیه خود را آماده‌سازی نموده است و به ارائه محصول نهایی خود پرداخته است.</p>
            </div>
        </div>
    </div>

    <div class="container-fluid col-lg-12 col-md-12 my-5" style="background-color:#929292;">
                <div class="row justify-content-center mt-5"style="height: 50%;">
                        <div class="col-2  d-flex mx-1 mt-5" style="height: 100%;display: block"><img src="/img/هوشمندسازی.png" class="col-10 img-fluid" alt=""> </div>
                        <div class="col-2  d-flex mx-1 mt-5" style="height: 100%;display: block"><img src="/img/اپلیکیشن.png" class="col-10 img-fluid" alt=""></div>
                        <div class="col-2  d-flex mx-1 mt-5" style="height: 100%;display: block"><img src="/img/انتخاب%20گیاه.png" class="col-10 img-fluid" alt=""></div>
                        <div class="col-2  d-flex mx-1 mt-5" style="height: 100%;display: block"><img src="/img/انتخاب%20اسکلت%20بندی.png" class="col-10 img-fluid" alt=""></div>
                        <div class="col-2  d-flex mx-1 mt-5" style="height: 100%;display: block"><img src="/img/هوشمندسازی.png" class="col-10 img-fluid" alt=""></div>
                </div>
    </div>
    <div class="container col-lg-12 justify-content-center my-5 mx-auto" style="">
        <h1 class="text-center">بیشتر با رستمانا آشنا شوید</h1>
        <video class=" col-6 mx-auto justify-content-center d-flex  border " controls >
            <source class="col-6 mx-auto justify-content-center d-flex " src="./video/rostmana video.mp4" type="video/mp4">
{{--            <source src="./video/rostmana video.mp4" type="video/ogg">--}}
        </video>
        <div class=" mx-auto col-12 text-center mt-5" style=""> <h3>بیشتر با رست‌مانا آشنا شوید</h3>
            <br>
            رٌست مانا با تولید گلخانه‌های هوشمند ملی و هوشمندسازی فضاهای سبز داخلی، گامی مؤثر را در جهت تلفیق هنر و طبیعت، با ارئه طراحی و سبکی نوین در دکوراسیون داخلی بر‌می‌دارد.
        </div>
    </div>
    <div class="container-fluid  col-lg-12" style=""id="back-info">
        <div class="col-12 mx-auto bg-light py-2 px-5" style="margin-top: 100px;border-radius: 20px">
        <h1 class="font-weight-bold mr-5  mt-5">محل کار ما</h1>
            <p class="font-weight-light mt-3" style="font-size: 1.3rem">

                تیم رٌست مانا در مجموعه شتابدهی نکسترا فعالیت خود را آغاز نموده است. روحیه کار تیمی و ارتباطات مؤثر در محیط کاری  سبب شده است تا شاهد موفقیت‌های چشم‌گیر در زمینه‌های مختلف برای ما فراهم گردد. جذابیت محیط کار و همدلی اعضاء تیم، عاملی بر توسعه کسب و کار ما می‌باشد. در حال حاضر افراد متخصص در حوزه‌های مختلف برای توسعه خدمات و بهبود کیفیت محصولات رست‌مانا در تلاش می‌باشند.
            </p>
        </div>
        <div class="row col-12  mx-auto justify-content-center" id="P_Profile" >
            <div class="col-lg-3 col-sm-12  mx-5 bg-light my-5 justify-content-center "style="">

                <div class=" col-6 mx-auto mt-5 " style="border-radius: 20px;"><img src="../Profile/تینا%20حسن%20ولی.JPG" class="img-fluid shadow  " style="border-radius: 20px" alt=""></div>

                <h1 class="text-center mt-5">تینا حسن ولی</h1>
                <h5 class="text-center">هم‌بنیان‌گذار و مدیر فنی</h5>
                <h6 class="text-center">کارشناس ارشد مکانیک - طراحی جامدات</h6>

            </div>
            <div class="col-lg-3 col-sm-12 mx-5 bg-light my-5" style="">
                <div class=" col-6 mx-auto mt-5 " style="border-radius: 20px;"><img src="../Profile/ادیبه%20چوپانی.JPG" class="img-fluid shadow  " style="border-radius: 20px" alt=""></div>
                <h1 class="text-center mt-5">ادیبه چوپانی</h1>
                <h5 class="text-center">هم‌بنیان‌گذار و مدیر طراحی</h5>
                <h6 class="text-center">کارشناس ارشد مکانیک - طراحی جامدات</h6>
            </div>
            <div class="col-lg-3 col-sm-12 mx-5 bg-light my-5" style="">
                <div class=" col-6 mx-auto mt-5 " style="border-radius: 20px;"><img src="../Profile/ابولفضل%20رحمانی.jpg" class="img-fluid shadow  " style="border-radius: 20px" alt=""></div>
                <h1 class="text-center mt-5">ابوالفضل رحمانی</h1>
                <h5 class="text-center">WebDeveloper</h5>
                <h6 class="text-center">مهندسی کامپیوتر - نرم‌افزار</h6>
            </div>
            <div class="col-lg-3 col-sm-12 mx-5 bg-light my-5" style="">
                <div class=" col-6 mx-auto mt-5 " style="border-radius: 20px;"><img src="../Profile/عرفان%20اخوان%20راد.jpeg" class="img-fluid shadow  " style="border-radius: 20px" alt=""></div>
                <h1 class="text-center mt-5">عرفان اخوان‌ راد</h1>
                <h5 class="text-center">AppDeveloper</h5>
                <h6 class="text-center">مهندسی کامپیوتر - نرم‌افزار</h6>
            </div>
            <div class="col-lg-3 col-sm-12 mx-5 bg-light my-5" style="">
             <div class="col-6 mx-auto mt-5 " style=""><img src="../Profile/محمد%20صابر%20فهیمی.jpg" class="img-fluid shadow  " style="border-radius: 20px" alt=""></div>
                <h1 class="text-center mt-5">محمدصابر فهیمی</h1>
                <h5 class="text-center">سرپرست الکترونیک</h5>
                <h6 class="text-center">مهندسی مکانیک</h6>
            </div>
            <div class="col-lg-3 col-sm-12 mx-5 bg-light my-5" style="">
                <div class=" col-6 mx-auto mt-5 " style="border-radius: 20px;"><img src="/Profile/بابک%20صادقی.jpeg" class="img-fluid shadow  " style="border-radius: 20px" alt=""></div>
                <h1 class="text-center mt-5">بابک صادقی‌نسب</h1>
                <h5 class="text-center ">سرپرست کارگاه ساخت</h5>
                <h6 class="text-center">مهندسی مکانیک - ساخت و تولید</h6>

            </div>


        </div>



<div>
</div>




@endsection
