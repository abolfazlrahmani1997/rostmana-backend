<?php

namespace App\Http\Controllers;

use App\Categoryfertilizer;
use App\CategoryPlant;
use App\Categorysoil;
use App\Plant;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\File;
use Yajra\DataTables\DataTables;

class PlantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()){
            $data = Plant::with('CategoryPlant','fertilizer','soil')->get();
            return Datatables::of($data)->make(true);
        }
        return view('Dashbord.Plant.index')->with('Plants',Plant::all());
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Dashbord.Plant.form')->with('Planttypes',CategoryPlant::all())->with('soils',Categorysoil::all())->with('fertilizers',Categoryfertilizer::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $plant=Plant::create($request->all());
        if($request->file()) {

        $image = $request->file('photo');
        $name = time() . '.' . $image->getClientOriginalExtension();
        $destinationPath = public_path('/Plant');
        $image->move($destinationPath, $name);
        $plant->photo=$name;

    }else{
            $request->photo='defualt';
    }

//        $plant->photo=$name;
        $plant->save();
        return view('Dashbord.Plant.index')->with('Plants',Plant::all());
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Plant  $plant
     * @return \Illuminate\Http\Response
     */
    public function show(Plant $plant)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Plant  $plant
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $plant=Plant::find($id);

        return view('Dashbord.Plant.form')->with('Plant',$plant)->with('Planttypes',CategoryPlant::all())->with('soils',Categorysoil::all())->with('fertilizers',Categoryfertilizer::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Plant  $plant
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Plant $plant)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Plant  $plant
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Plant=Plant::find($id);
//        \Illuminate\Support\Facades\File::delete(public_path( $Plant->photo));
        $Plant->Delete();
        return redirect()->route('Plant.index');
    }
}
