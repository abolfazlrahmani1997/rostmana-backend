<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plant extends Model
{
    protected $fillable=['planttype','name','temp','photo','light','humidity_soil','humidity_air','soil_id','fertilizer_id'];
    protected $hidden=['builder_id','updated_at','id','created_at','updated_at'];
    protected  $path="/Plant/";
    public function getPhotoAttribute($value)
    {
        if($value!=''){
            return $this->path.$value;
        }
        return $value;
    }
    public function CategoryPlant(){
        return $this->hasOne('App\CategoryPlant','id','planttype');
    }
    public function fertilizer(){
        return $this->hasOne('App\Categoryfertilizer','id','fertilizer_id');
    }
    public function soil(){
        return $this->hasOne('App\Categorysoil','id','soil_id');
    }
}
