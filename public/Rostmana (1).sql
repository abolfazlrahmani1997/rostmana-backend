-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 19, 2021 at 01:28 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Rostmana`
--

-- --------------------------------------------------------

--
-- Table structure for table `automodes`
--

CREATE TABLE `automodes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `automodes`
--

INSERT INTO `automodes` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'fullauto', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `builders`
--

CREATE TABLE `builders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `width` double(8,2) NOT NULL,
  `length` double(8,2) NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender_id` int(10) UNSIGNED NOT NULL,
  `automode_id` int(10) UNSIGNED NOT NULL,
  `watermode_id` int(10) UNSIGNED NOT NULL,
  `picture` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `plant_id` bigint(20) UNSIGNED NOT NULL,
  `qrcode` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `builders`
--

INSERT INTO `builders` (`id`, `name`, `model`, `width`, `length`, `icon`, `gender_id`, `automode_id`, `watermode_id`, `picture`, `created_at`, `updated_at`, `plant_id`, `qrcode`) VALUES
(4, 'rostmana', 'rm-456', 10.00, 10.00, '1608538786.jpg', 1, 1, 1, 1, NULL, NULL, 3, '1233213'),
(5, 'banamana', 'ba-465', 1.00, 1.00, '1608538786.jpg', 1, 1, 1, 1, '2020-12-21 04:49:47', '2020-12-21 04:49:47', 3, '123456'),
(7, 'cc', 'ccc', 1.00, 1.00, '1608555237.jpg', 1, 1, 1, 1, '2020-12-21 09:23:57', '2020-12-21 09:23:57', 3, '1');

-- --------------------------------------------------------

--
-- Table structure for table `builder_user`
--

CREATE TABLE `builder_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `builder_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `builder_user`
--

INSERT INTO `builder_user` (`id`, `user_id`, `builder_id`) VALUES
(4, 55, 4),
(5, 55, 5),
(6, 55, 6),
(7, 55, 7);

-- --------------------------------------------------------

--
-- Table structure for table `cache`
--

CREATE TABLE `cache` (
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `expiration` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cache`
--

INSERT INTO `cache` (`key`, `value`, `expiration`) VALUES
('laravel_cache10', 's:3:\"123\";', 1610965510),
('laravel_cache55', 's:6:\"944319\";', 1926330745),
('laravel_cache8effee409c625e1a2d8f5033631840e6ce1dcb64', 'i:1;', 1610974561),
('laravel_cache8effee409c625e1a2d8f5033631840e6ce1dcb64:timer', 'i:1610974561;', 1610974561);

-- --------------------------------------------------------

--
-- Table structure for table `categoryfertilizers`
--

CREATE TABLE `categoryfertilizers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `fertilizer_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categoryfertilizers`
--

INSERT INTO `categoryfertilizers` (`id`, `fertilizer_type`, `created_at`, `updated_at`) VALUES
(1, 'حیوانی', NULL, NULL),
(2, 'good', '2020-12-06 09:47:49', '2020-12-06 09:47:49');

-- --------------------------------------------------------

--
-- Table structure for table `categorysoils`
--

CREATE TABLE `categorysoils` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `soil` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categorysoils`
--

INSERT INTO `categorysoils` (`id`, `name`, `created_at`, `updated_at`, `soil`) VALUES
(1, 'برگ', NULL, NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `category_plants`
--

CREATE TABLE `category_plants` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `TypePlants` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `category_plants`
--

INSERT INTO `category_plants` (`id`, `TypePlants`, `created_at`, `updated_at`) VALUES
(1, 'زینتی', NULL, NULL),
(2, 'کاکتوس', '2020-12-02 09:48:48', '2020-12-02 09:48:48');

-- --------------------------------------------------------

--
-- Table structure for table `comands`
--

CREATE TABLE `comands` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `builder_id` bigint(20) UNSIGNED NOT NULL,
  `fan_status` tinyint(1) NOT NULL DEFAULT 0,
  `vap_status` tinyint(1) NOT NULL DEFAULT 0,
  `fogg_status` tinyint(1) NOT NULL DEFAULT 0,
  `light_status` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `genders`
--

CREATE TABLE `genders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `genders`
--

INSERT INTO `genders` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'wood', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` int(10) UNSIGNED NOT NULL,
  `temp` double(8,2) UNSIGNED NOT NULL,
  `light` double(8,2) UNSIGNED NOT NULL,
  `humidity_soil` double(8,2) UNSIGNED NOT NULL,
  `humidity_air` double(8,2) UNSIGNED NOT NULL,
  `water_level` float NOT NULL,
  `fan_status` tinyint(1) NOT NULL,
  `fogger_status` tinyint(1) NOT NULL,
  `builder_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`id`, `name`, `temp`, `light`, `humidity_soil`, `humidity_air`, `water_level`, `fan_status`, `fogger_status`, `builder_id`, `created_at`, `updated_at`) VALUES
(3, 1, 10.00, 2.00, 10.00, 10.00, 0, 1, 1, 7, '2020-11-25 09:27:03', '2020-11-25 09:27:03'),
(4, 1, 10.00, 10.00, 10.00, 10.00, 0, 1, 1, 7, '2020-11-25 09:29:26', '2020-11-25 09:29:26'),
(5, 1, 10.00, 10.00, 10.00, 10.00, 0, 1, 1, 7, '2020-11-25 09:29:26', '2020-11-25 09:29:26'),
(6, 1, 10.00, 10.00, 10.00, 10.00, 0, 1, 1, 7, '2020-11-25 10:19:37', '2020-11-25 10:19:37'),
(7, 1, 10.00, 10.00, 10.00, 10.00, 0, 1, 1, 7, '2020-11-25 10:20:18', '2020-11-25 10:20:18'),
(8, 1, 10.00, 10.00, 10.00, 10.00, 0, 1, 1, 7, '2020-11-25 10:22:25', '2020-11-25 10:22:25'),
(9, 1, 10.00, 10.00, 10.00, 10.00, 0, 1, 1, 7, '2020-11-25 10:22:50', '2020-11-25 10:22:50'),
(10, 1, 10.00, 10.00, 10.00, 10.00, 0, 1, 1, 7, '2020-11-25 10:23:30', '2020-11-25 10:23:30'),
(11, 1, 10.00, 10.00, 10.00, 11.00, 0, 1, 1, 7, '2020-11-25 10:28:59', '2020-11-25 10:28:59'),
(12, 1, 12.00, 10.00, 10.00, 11.00, 0, 1, 1, 7, '2020-11-28 05:52:04', '2020-11-28 05:52:04'),
(13, 1, 10.00, 10.00, 10.00, 11.00, 100, 1, 1, 7, '2020-12-30 01:26:52', '2020-12-30 01:26:52'),
(14, 1, 10.00, 10.00, 10.00, 11.00, 100, 1, 1, 7, '2020-12-30 03:52:48', '2020-12-30 03:52:48'),
(15, 1, 10.00, 10.00, 10.00, 11.00, 80, 1, 1, 7, '2020-12-30 03:53:05', '2020-12-30 03:53:05'),
(16, 1, 10.00, 10.00, 10.00, 11.00, 70, 1, 1, 7, '2020-12-30 03:56:23', '2020-12-30 03:56:23'),
(17, 1, 10.00, 10.00, 10.00, 11.00, 128, 1, 1, 7, '2020-12-30 05:14:22', '2020-12-30 05:14:22'),
(18, 1, 10.00, 10.00, 10.00, 11.00, 128, 1, 1, 7, '2020-12-30 05:14:32', '2020-12-30 05:14:32'),
(19, 1, 10.00, 10.00, 10.00, 11.00, 128, 1, 1, 7, '2020-12-30 05:14:42', '2020-12-30 05:14:42'),
(20, 1, 10.00, 10.00, 10.00, 11.00, 128, 1, 1, 7, '2020-12-30 05:14:53', '2020-12-30 05:14:53'),
(21, 1, 10.00, 10.00, 10.00, 11.00, 128, 1, 1, 7, '2020-12-30 05:15:03', '2020-12-30 05:15:03'),
(22, 1, 10.00, 10.00, 10.00, 11.00, 128, 1, 1, 7, '2020-12-30 05:15:13', '2020-12-30 05:15:13'),
(23, 1, 10.00, 10.00, 10.00, 11.00, 128, 1, 1, 7, '2020-12-30 05:15:23', '2020-12-30 05:15:23'),
(24, 1, 10.00, 10.00, 10.00, 11.00, 128, 1, 1, 7, '2020-12-30 05:15:33', '2020-12-30 05:15:33'),
(25, 1, 10.00, 10.00, 10.00, 11.00, 128, 1, 1, 7, '2020-12-30 05:15:43', '2020-12-30 05:15:43'),
(26, 1, 10.00, 10.00, 10.00, 11.00, 128, 1, 1, 7, '2020-12-30 05:15:53', '2020-12-30 05:15:53'),
(27, 1, 10.00, 10.00, 10.00, 11.00, 128, 1, 1, 7, '2020-12-30 05:16:03', '2020-12-30 05:16:03'),
(28, 1, 10.00, 10.00, 10.00, 11.00, 128, 1, 1, 7, '2020-12-30 05:16:13', '2020-12-30 05:16:13'),
(29, 1, 10.00, 10.00, 10.00, 11.00, 128, 1, 1, 7, '2020-12-30 05:16:23', '2020-12-30 05:16:23'),
(30, 1, 10.00, 10.00, 10.00, 11.00, 128, 1, 1, 7, '2020-12-30 05:16:33', '2020-12-30 05:16:33'),
(31, 1, 10.00, 10.00, 10.00, 11.00, 128, 1, 1, 7, '2020-12-30 05:16:43', '2020-12-30 05:16:43'),
(32, 1, 10.00, 10.00, 10.00, 11.00, 128, 1, 1, 7, '2020-12-30 05:16:54', '2020-12-30 05:16:54'),
(33, 1, 10.00, 10.00, 10.00, 11.00, 128, 1, 1, 7, '2020-12-30 05:17:04', '2020-12-30 05:17:04'),
(34, 1, 10.00, 10.00, 10.00, 11.00, 128, 1, 1, 7, '2020-12-30 05:17:14', '2020-12-30 05:17:14'),
(35, 1, 10.00, 10.00, 10.00, 11.00, 128, 1, 1, 7, '2020-12-30 05:17:24', '2020-12-30 05:17:24'),
(36, 1, 10.00, 10.00, 10.00, 11.00, 128, 1, 1, 7, '2020-12-30 05:17:34', '2020-12-30 05:17:34'),
(37, 1, 10.00, 10.00, 10.00, 11.00, 128, 1, 1, 7, '2020-12-30 05:17:44', '2020-12-30 05:17:44'),
(38, 1, 10.00, 10.00, 10.00, 11.00, 128, 1, 1, 7, '2020-12-30 05:17:54', '2020-12-30 05:17:54'),
(39, 1, 10.00, 10.00, 10.00, 11.00, 128, 1, 1, 7, '2020-12-30 05:18:04', '2020-12-30 05:18:04'),
(40, 1, 10.00, 10.00, 10.00, 11.00, 128, 1, 1, 7, '2020-12-30 05:18:14', '2020-12-30 05:18:14'),
(41, 1, 10.00, 10.00, 10.00, 11.00, 128, 1, 1, 7, '2020-12-30 05:18:24', '2020-12-30 05:18:24'),
(42, 1, 10.00, 10.00, 10.00, 11.00, 128, 1, 1, 7, '2020-12-30 05:27:24', '2020-12-30 05:27:24'),
(43, 1, 10.00, 10.00, 10.00, 11.00, 128, 1, 1, 7, '2020-12-30 05:27:34', '2020-12-30 05:27:34'),
(44, 1, 10.00, 10.00, 10.00, 11.00, 128, 1, 1, 7, '2020-12-30 05:27:44', '2020-12-30 05:27:44'),
(45, 1, 10.00, 10.00, 10.00, 11.00, 128, 1, 1, 7, '2020-12-30 05:27:54', '2020-12-30 05:27:54'),
(46, 1, 10.00, 10.00, 10.00, 11.00, 128, 1, 1, 7, '2020-12-30 05:28:05', '2020-12-30 05:28:05'),
(47, 1, 10.00, 10.00, 10.00, 11.00, 128, 1, 1, 7, '2020-12-30 05:28:15', '2020-12-30 05:28:15'),
(48, 1, 10.00, 10.00, 10.00, 11.00, 128, 1, 1, 7, '2020-12-30 05:28:25', '2020-12-30 05:28:25'),
(49, 1, 10.00, 10.00, 10.00, 11.00, 128, 1, 1, 7, '2020-12-30 05:28:35', '2020-12-30 05:28:35'),
(50, 1, 10.00, 10.00, 10.00, 11.00, 128, 1, 1, 7, '2020-12-30 05:28:45', '2020-12-30 05:28:45'),
(51, 1, 10.00, 10.00, 10.00, 11.00, 128, 1, 1, 7, '2020-12-30 05:28:55', '2020-12-30 05:28:55'),
(52, 1, 10.00, 10.00, 10.00, 11.00, 128, 1, 1, 7, '2020-12-30 05:29:05', '2020-12-30 05:29:05'),
(53, 1, 10.00, 10.00, 10.00, 11.00, 128, 1, 1, 7, '2020-12-30 05:29:15', '2020-12-30 05:29:15'),
(54, 1, 10.00, 10.00, 10.00, 11.00, 100, 1, 1, 7, '2021-01-03 04:16:26', '2021-01-03 04:16:26'),
(59, 1, 10.00, 10.00, 10.00, 11.00, 128, 1, 1, 7, '2021-01-06 03:55:18', '2021-01-06 03:55:18'),
(60, 1, 10.00, 10.00, 10.00, 11.00, 128, 1, 1, 7, '2021-01-06 03:55:28', '2021-01-06 03:55:28'),
(61, 1, 10.00, 10.00, 10.00, 11.00, 128, 1, 1, 7, '2021-01-06 04:20:47', '2021-01-06 04:20:47');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(8, '2019_08_19_000000_create_failed_jobs_table', 1),
(9, '2020_11_16_120754_create_profiles_table', 1),
(10, '2020_11_16_134533_create_builders_table', 1),
(11, '2020_11_21_115554_create_automodes_table', 1),
(12, '2020_11_21_115611_create_genders_table', 1),
(13, '2020_11_21_115632_create_watermodes_table', 1),
(14, '2020_11_21_115649_create_pictures_table', 1),
(15, '2020_11_21_122649_create_builder_user', 1),
(16, '2020_11_22_122847_create_logs_table', 2),
(18, '2020_11_23_090713_create_category_plants_table', 3),
(19, '2020_11_23_090740_create_categoryfertilizers_table', 3),
(28, '2020_11_23_082234_create_plants_table', 4),
(29, '2020_11_24_090718_add_plantid_to_builders_table', 5),
(30, '2020_11_24_102835_add_qrcode_to_builders_table', 6),
(31, '2020_11_24_121743_add_name_to_categorysoil_table', 7),
(32, '2020_11_24_122102_add_soil_to_categorysoil_table', 8),
(38, '2020_12_21_101405_create_comands_table', 9),
(39, '2021_01_18_101924_create_cache_table', 9);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('029905c41a0563cd829ad3d035d30e1495ae4dd668e8255a0d95ade74796887335df007adae6c098', 13, 1, 'Personal Access Token', '[]', 0, '2020-12-12 11:09:33', '2020-12-12 11:09:33', '2021-12-12 14:39:33'),
('043287cdf1d5d8103025015e74e0667f530585d213d5af661f39ae99bab28a01d2f07b7e609b1df6', 55, 1, 'Personal Access Token', '[]', 0, '2020-12-26 10:51:07', '2020-12-26 10:51:07', '2021-12-26 14:21:07'),
('0438fd48172fd257cdab32d53ee6169719dfb40924cb3c5a467139498061c3c25bbcb310aaec707f', 55, 1, 'Personal Access Token', '[]', 0, '2020-12-26 09:37:40', '2020-12-26 09:37:40', '2021-12-26 13:07:40'),
('047a4b63cb0c653d262ae02a1428fce03f293fccd494463ae69215b1bfbcbd5228574aa6ae3a562c', 13, 1, 'Personal Access Token', '[]', 0, '2020-12-13 10:07:42', '2020-12-13 10:07:42', '2021-12-13 13:37:42'),
('060f32bf41bdbfa1307d91d8c0d5881c32072106708afa302a82b2474f226dee010810dc90f94d25', 55, 1, 'Personal Access Token', '[]', 0, '2021-01-16 11:57:11', '2021-01-16 11:57:11', '2022-01-16 15:27:11'),
('0708a2fce739128cfbad824723d8d73f2965ce7e9f34159687ff9ab6ee2f2c3c356821dae460bb31', 55, 1, 'Personal Access Token', '[]', 0, '2021-01-16 05:53:15', '2021-01-16 05:53:15', '2022-01-16 09:23:15'),
('079cc42ef546873fb3d689d5f4ac78f606f900ab22140d39cdaf8e9389e1531e6b9713c873a5bf45', 13, 1, 'Personal Access Token', '[]', 0, '2020-12-12 09:36:00', '2020-12-12 09:36:00', '2021-12-12 13:06:00'),
('0903f2f8c010d41d03d2c21ffd569cbd95d55cb9b60da52f12a4621b2ce820bc885d8d5308ec5cb0', 55, 1, 'Personal Access Token', '[]', 0, '2020-12-27 05:51:30', '2020-12-27 05:51:30', '2021-12-27 09:21:30'),
('0a753ec271f984a566db321767123122f393132e434d673adf752edd583167f1860376560a114d8b', 55, 1, 'Personal Access Token', '[]', 0, '2020-12-26 10:54:57', '2020-12-26 10:54:57', '2021-12-26 14:24:57'),
('0ae61e7b51dc73997e02edcf08ddeca183e436c5bd15893a54c2796aea5c61dbe79a0d0da26a8d9b', 52, 1, 'NewToken', '[]', 0, '2020-12-16 10:21:02', '2020-12-16 10:21:02', '2021-12-16 13:51:02'),
('0dfdaa3676126e14de481a6af29ab340077d34300e5a4ba11dd753caa7fa404f4c97a4da58f92017', 55, 1, 'Personal Access Token', '[]', 0, '2021-01-16 09:19:20', '2021-01-16 09:19:20', '2022-01-16 12:49:20'),
('0e4ea3747f0565962bfec348811b583326485e413d1010c8c7d8f9d2e95478b3a362bffdfb836ab0', 13, 1, 'Personal Access Token', '[]', 0, '2020-12-12 09:35:58', '2020-12-12 09:35:58', '2021-12-12 13:05:58'),
('0f6243b68876d4cd07eab8c4163d33d849165e7dff6a21342386d7ba1771d312b5bcff2884314143', 55, 1, 'Personal Access Token', '[]', 0, '2020-12-26 09:43:46', '2020-12-26 09:43:46', '2021-12-26 13:13:46'),
('14d03af6a190cdb43230ddbb2f4341f57b8dc244efcbfe52ee923dd9d199efbe76948c71593121c4', 55, 1, 'Personal Access Token', '[]', 0, '2021-01-18 08:54:40', '2021-01-18 08:54:40', '2022-01-18 12:24:40'),
('15db42cc1c5047c5088336913bc41fe4ab73cff2abcc92c6c52ece77ca931728fb64bcc1fd516ad1', 13, 1, 'Personal Access Token', '[]', 0, '2020-12-01 05:37:15', '2020-12-01 05:37:15', '2021-12-01 09:07:15'),
('180d9514cae3408acd0579969b50108e5e81cc0778a649a46e88079baf42d4d02baf0ace2c23b734', 13, 1, 'Personal Access Token', '[]', 0, '2020-12-13 08:24:43', '2020-12-13 08:24:43', '2021-12-13 11:54:43'),
('1851b15767cecc3cf602f2656b51e9c79fb52a53bbee3621afa1e8125c9476cc05cca0d98dc11a52', 13, 1, 'Personal Access Token', '[]', 0, '2020-12-12 04:57:20', '2020-12-12 04:57:20', '2021-12-12 08:27:20'),
('191ca5895b7cd633ff532a599001c74658adce9686e35f0c749f8569ed5f6608e672da6c27199d5c', 55, 1, 'Personal Access Token', '[]', 0, '2021-01-16 12:03:52', '2021-01-16 12:03:52', '2022-01-16 15:33:52'),
('1e77bf5452ef78409db00d3dcc91577f2c5660d65fdcc7938f2422f6b762282a78122c020b631824', 55, 1, 'Personal Access Token', '[]', 0, '2020-12-27 05:46:09', '2020-12-27 05:46:09', '2021-12-27 09:16:09'),
('21c245ac74fd80a8d38cace16db5a925a1172adb80889bc1b2f995bfb8752d13f7514b4bbda403d3', 13, 1, 'Personal Access Token', '[]', 0, '2020-12-12 09:36:47', '2020-12-12 09:36:47', '2021-12-12 13:06:47'),
('22dd6aa5fe4100b1c4c7f28567240ea66b7a670f36a71d48983fa7b9de1471ebea8b7523eafb991b', 55, 1, 'Personal Access Token', '[]', 0, '2020-12-26 10:54:00', '2020-12-26 10:54:00', '2021-12-26 14:24:00'),
('2663483d98e0abdef1304c78487e7498f0dacb41dd4d2823429b55b057d4145823eb6e10f7fa4df6', 55, 1, 'Personal Access Token', '[]', 0, '2021-01-16 10:38:59', '2021-01-16 10:38:59', '2022-01-16 14:08:59'),
('2695babb45aa07bae0890c8d3879caebb8e93973892ec9f35f3ee8538a61a15043136b0f410f7bcc', 13, 1, 'Personal Access Token', '[]', 0, '2020-12-12 10:47:34', '2020-12-12 10:47:34', '2021-12-12 14:17:34'),
('270f3c2a778b3d90792ea0568a6446799a8ece93f04dfd3384c820daa1499cbfad9d922125791321', 13, 1, 'Personal Access Token', '[]', 0, '2020-12-12 05:28:24', '2020-12-12 05:28:24', '2021-12-12 08:58:24'),
('2c276b824094b8415755f912ddae2e0b83baf71aeb29bc945b40f13d206be308ed821c53c00162b1', 55, 1, 'Personal Access Token', '[]', 0, '2021-01-18 06:08:42', '2021-01-18 06:08:42', '2022-01-18 09:38:42'),
('2d9433997fd87e508baa443b0eb88240c133f93c13fecad5acc9d7ee0c20610a76024470046b0b09', 13, 1, 'Personal Access Token', '[]', 0, '2020-12-01 10:30:31', '2020-12-01 10:30:31', '2021-12-01 14:00:31'),
('2ebfea31524eefe49ed9c39cc4bb8ec02b883c54474acce0003e6d06cd9ef986bc8ae8dd54046584', 54, 1, 'NewToken', '[]', 0, '2020-12-16 10:22:28', '2020-12-16 10:22:28', '2021-12-16 13:52:28'),
('3518d93388ee5e3ea51138c67936aa313a86270bbc162891536bc3ed1aa4cc1a9ab727ca42c6f05f', 55, 1, 'Personal Access Token', '[]', 0, '2021-01-16 11:57:15', '2021-01-16 11:57:15', '2022-01-16 15:27:15'),
('3624cd123288f5b1224c071d540c064b9ef03ac8f11c8646f5b0b33868822e6e8c6d35b9247180e1', 1, 1, 'Personal Access Token', '[]', 0, '2020-11-30 05:43:15', '2020-11-30 05:43:15', '2021-11-30 09:13:15'),
('368ff033ffe4b431a61366e3ff9549980b6c59d9803ae58e1dce6e085fbc7902ccfb53e99c0cdced', 55, 1, 'Personal Access Token', '[]', 0, '2021-01-18 06:08:41', '2021-01-18 06:08:41', '2022-01-18 09:38:41'),
('3cedad87ba15fe7bf654f75950cd60794beb9a54257ad1d2b8f935c5d9af31800929004a7ce4169c', 55, 1, 'Personal Access Token', '[]', 0, '2020-12-27 05:44:25', '2020-12-27 05:44:25', '2021-12-27 09:14:25'),
('42a6569f0ccf897541c8fd29f554f8a612544832dea33be13e097336fdb0f26f96fb9c8fb2433c73', 55, 1, 'Personal Access Token', '[]', 0, '2021-01-16 12:34:19', '2021-01-16 12:34:19', '2022-01-16 16:04:19'),
('44b0da0398ccf0ec76fcb57777ace47b5a649d0e8534a8f4b6af7ccf01d8bc9f530190885bfd7e7b', 55, 1, 'Personal Access Token', '[]', 0, '2020-12-27 04:20:52', '2020-12-27 04:20:52', '2021-12-27 07:50:52'),
('45b96142e7dfe0be1dd548195d9bdea9e7a4c140598525eb013267526710b072e11fde0d72ebf526', 13, 1, 'Personal Access Token', '[]', 0, '2020-12-12 10:59:01', '2020-12-12 10:59:01', '2021-12-12 14:29:01'),
('47fcdbbf5b72ed115a971c32ed21f356cc6ceffb927582a300cc4f13edfd0a826565a38baf9c3c69', 55, 1, 'Personal Access Token', '[]', 0, '2020-12-27 05:52:29', '2020-12-27 05:52:29', '2021-12-27 09:22:29'),
('49df40f5fa9a9e5a0bb82f28ce9a089589fd6ad314069dc93844b27ca9d57fe2b2fed3b7a538f63c', 13, 1, 'Personal Access Token', '[]', 0, '2020-12-09 11:02:15', '2020-12-09 11:02:15', '2021-12-09 14:32:15'),
('4c3967450d2dc0c19566bc64cdedfe6988056f9fb51c856c50e15559a36b71802988817fd154d23f', 55, 1, 'Personal Access Token', '[]', 0, '2020-12-27 04:23:05', '2020-12-27 04:23:05', '2021-12-27 07:53:05'),
('4c4264a3b306d74c72c3820771d32e24b11e068dfec2a8042997257b810c1706b6ca17890f6e018d', 13, 1, 'Personal Access Token', '[]', 0, '2020-11-30 09:23:07', '2020-11-30 09:23:07', '2021-11-30 12:53:07'),
('52cf4df0eccedbcec2203c72f8dd12427bf95d14d3b2042ca74825543769a661172aa0cc1d93f236', 55, 1, 'Personal Access Token', '[]', 0, '2020-12-26 09:31:23', '2020-12-26 09:31:23', '2021-12-26 13:01:23'),
('58f4204102bc079a93eadb88362170daaf3c814b57239624ca4038303ae04254ff82653a87d8806a', 13, 1, 'Personal Access Token', '[]', 0, '2020-11-30 09:46:36', '2020-11-30 09:46:36', '2021-11-30 13:16:36'),
('5ddd6ba824354607e2a13ce8d8c66489102a81a4fcf791c995c8ea8757e73dde4387947efb3c1938', 55, 1, 'Personal Access Token', '[]', 0, '2021-01-16 07:02:09', '2021-01-16 07:02:09', '2022-01-16 10:32:09'),
('5ff73d6f1c8f74896fdf7c7ebf2a7db0182fd15aae383b43c74dda03e7eba022626f9306ae0fb989', 13, 1, 'Personal Access Token', '[]', 0, '2020-12-14 09:24:44', '2020-12-14 09:24:44', '2021-12-14 12:54:44'),
('6106aabf87845613f0fa3c879eb6f42b36fad07524ec736806ab54e09b24660cd1eed27d0f2ae211', 13, 1, 'Personal Access Token', '[]', 0, '2020-12-09 09:37:32', '2020-12-09 09:37:32', '2021-12-09 13:07:32'),
('6112eb6f0f78480870c451735af426af0700bdbb5e4b9edfbc39aa0cb504235d8d84365dc543385a', 55, 1, 'Personal Access Token', '[]', 0, '2021-01-16 05:51:01', '2021-01-16 05:51:01', '2022-01-16 09:21:01'),
('637f9a432329d0894a9bae5e654bde3d432c2a4de03b93e0ab721b1ce9d1235a8149d6e4ede07616', 55, 1, 'Personal Access Token', '[]', 0, '2021-01-16 05:50:55', '2021-01-16 05:50:55', '2022-01-16 09:20:55'),
('67dce58467103a88d6715d036fbd4a962745fbb267baf278c6bba7c54875c5846fbfc6b81a532b53', 13, 1, 'Personal Access Token', '[]', 0, '2020-12-01 10:28:05', '2020-12-01 10:28:05', '2021-12-01 13:58:05'),
('6914762d5bdacd79f0bc0463f4ee17f2e2fa4c111cbf5db523acfa355ba3a9c516666ef541b75310', 55, 1, 'Personal Access Token', '[]', 0, '2020-12-27 05:33:59', '2020-12-27 05:33:59', '2021-12-27 09:03:59'),
('69e2cd512090f2cc240c8d5ff26ec9b136bc20a2fecaeba914aa60983ab3e368cb96673216ae8a86', 55, 1, 'Personal Access Token', '[]', 0, '2020-12-26 09:37:41', '2020-12-26 09:37:41', '2021-12-26 13:07:41'),
('6a4a1384e17f84af2e760d8c6959bb0b43d3372ac12593ce88af486cb40fec9ee971a9204cd8ea97', 55, 1, 'Personal Access Token', '[]', 0, '2020-12-27 09:07:12', '2020-12-27 09:07:12', '2021-12-27 12:37:12'),
('6e3342ddd22f8f34a72d7c6664db328bcd9c046055b6ea50acb1e6fd541be4ced74bcc32a1622057', 55, 1, 'Personal Access Token', '[]', 0, '2021-01-16 12:23:52', '2021-01-16 12:23:52', '2022-01-16 15:53:52'),
('71eaa4db8350c1c753508f378b8406faf87297030452f3578ead3d14e73f48a0a2201b75f9c2b108', 55, 1, 'Personal Access Token', '[]', 0, '2020-12-26 10:45:24', '2020-12-26 10:45:24', '2021-12-26 14:15:24'),
('72f6a77470175150aab78056a442f466ef9479e54199462ba31d964a7be0045336eeab92bdc324b0', 13, 1, 'Personal Access Token', '[]', 0, '2020-12-01 05:20:56', '2020-12-01 05:20:56', '2021-12-01 08:50:56'),
('76fa4ce899d04271cc8c67675850e83c083de79824e1ec23831a3d2277c8a8e58b27034c66f5ed9d', 55, 1, 'Personal Access Token', '[]', 0, '2020-12-26 09:33:37', '2020-12-26 09:33:37', '2021-12-26 13:03:37'),
('7752a4e34653c82e9848a60c9932b7dc3b5c203dceddd6617517a107124fc98c4c5610c17d4843fd', 13, 1, 'NewToken', '[]', 0, '2020-11-30 09:16:57', '2020-11-30 09:16:57', '2021-11-30 12:46:57'),
('798c0e2206364e81d2a8e9abe63459df2443a2c7053ac7660bbc11b68e5fe9e3195efe50f0cd8207', 55, 1, 'Personal Access Token', '[]', 0, '2020-12-27 05:50:44', '2020-12-27 05:50:44', '2021-12-27 09:20:44'),
('7aa52f6d8e0488c6c96b39f331b91ff20359022b309a5425343d2b97c0be7ee02a1e42d2a3678eef', 13, 1, 'Personal Access Token', '[]', 0, '2020-12-01 04:10:41', '2020-12-01 04:10:41', '2021-12-01 07:40:41'),
('7b8974d475f73655c6dc27bfc4a4a3d63fa1c0ad14fdd2d034ad68bb2520ac32a472300d5dc76444', 55, 1, 'Personal Access Token', '[]', 0, '2020-12-19 03:44:31', '2020-12-19 03:44:31', '2021-12-19 07:14:31'),
('7f209ac770ffaab273ed46e17ab68bd9d6126152a8bef9fbe54e98fe44ee41dc958f22fb3d926bb9', 55, 1, 'Personal Access Token', '[]', 0, '2021-01-18 05:41:04', '2021-01-18 05:41:04', '2022-01-18 09:11:04'),
('7faec15f63a0692920f39ab63c4baf5533ab4525f28d60ef4d0ade6ebb546ec36cb0fe7247cd974f', 13, 1, 'Personal Access Token', '[]', 0, '2020-12-01 04:26:19', '2020-12-01 04:26:19', '2021-12-01 07:56:19'),
('7fc467ccfe02aaeaba58bc236126d6880522e5a0f236f20f082501765aec9407cb06f5597db818dc', 13, 1, 'Personal Access Token', '[]', 0, '2020-12-10 05:52:05', '2020-12-10 05:52:05', '2021-12-10 09:22:05'),
('80e34178eea5e0135ee3f694851448daf84812e411f6e4df8e39b17319256189c725d225a6ad1c90', 55, 1, 'Personal Access Token', '[]', 0, '2020-12-26 09:45:07', '2020-12-26 09:45:07', '2021-12-26 13:15:07'),
('814893aded80ecbf6b0dc9f8098f2ee3e5748dd461a5812350440d829e9afcbb46a3d08f11c8a2ba', 13, 1, 'Personal Access Token', '[]', 0, '2020-12-12 09:36:28', '2020-12-12 09:36:28', '2021-12-12 13:06:28'),
('824b5bb7f8811a46dfdfc09c5f87490e21eaccc99b158536e3bb6b886a042f2690148afa7ebae408', 13, 1, 'Personal Access Token', '[]', 0, '2020-12-12 09:38:01', '2020-12-12 09:38:01', '2021-12-12 13:08:01'),
('88dafb0736b71d5417b031770c38cdd70f78415b16a9db9cb39edcac923b32e4edf473a334447a8c', 1, 1, 'Personal Access Token', '[]', 0, '2020-11-29 07:52:00', '2020-11-29 07:52:00', '2021-11-29 11:22:00'),
('90f8eb120f07257acb33f5f544e8a3a586fa323681ba345cdebd09026060f9c6688515d1ba6cb289', 13, 1, 'Personal Access Token', '[]', 0, '2020-12-12 04:36:00', '2020-12-12 04:36:00', '2021-12-12 08:06:00'),
('91d784bf98d6c06b9952c56ec53fa3ebf0c2fb8feb7a97668d666068684705dd7ebcc66ca138b1f1', 50, 1, 'NewToken', '[]', 0, '2020-12-16 10:13:30', '2020-12-16 10:13:30', '2021-12-16 13:43:30'),
('9d05316bf9bc20cba0ea5c3f713d841271f5cc7bcd88be262afdc5e9d83115cc4fafb2cf767b3264', 13, 1, 'Personal Access Token', '[]', 0, '2020-12-09 10:56:09', '2020-12-09 10:56:09', '2021-12-09 14:26:09'),
('9e3e63fff8b651d4708057ec28a1401afb5372d09f15fc7ef8717c4127992ce98966a3e5e7e4ffd1', 1, 1, 'NewToken', '[]', 0, '2020-11-21 09:45:46', '2020-11-21 09:45:46', '2021-11-21 13:15:46'),
('a069851589c1ca52818005fee2b72c26876f3fb0ee553475cb1585f998aee937d908768783eb82e4', 55, 1, 'Personal Access Token', '[]', 0, '2020-12-26 09:32:22', '2020-12-26 09:32:22', '2021-12-26 13:02:22'),
('a3260c0acec0659c804786be827220efde85a98f54ae5611f9fa8edec07f45a8ed8fcbb16dc8359f', 13, 1, 'Personal Access Token', '[]', 0, '2020-12-13 08:39:09', '2020-12-13 08:39:09', '2021-12-13 12:09:09'),
('a53f5e5fff4bad1e689d8e7a9998753d8a12087cfabf71e6cc3c20c3d5bc8799af609111e05563ea', 55, 1, 'Personal Access Token', '[]', 0, '2021-01-16 12:34:22', '2021-01-16 12:34:22', '2022-01-16 16:04:22'),
('a6bc453d10737bb6a2d2c408bf65e6d444527ded2f7094dd284e39375a612d61259e131674a3e787', 13, 1, 'Personal Access Token', '[]', 0, '2020-12-12 09:41:09', '2020-12-12 09:41:09', '2021-12-12 13:11:09'),
('a72c5e7d164c02dfbc23f472fb388cd600d13b3d6b51fc9255c8b61775826035502cd893290f888e', 55, 1, 'Personal Access Token', '[]', 0, '2020-12-19 04:38:07', '2020-12-19 04:38:07', '2021-12-19 08:08:07'),
('aab399ab5f7460f573384d818f7e6020853a2e4b98e65528f47f5e28b54ab1a1f6352ddd6df86e2e', 55, 1, 'Personal Access Token', '[]', 0, '2020-12-27 05:36:08', '2020-12-27 05:36:08', '2021-12-27 09:06:08'),
('ab9a86c85944847aeff7e7d8b25b1168490f582d6fb96165526c0f1a812bc0814de7de7036bdaa19', 13, 1, 'Personal Access Token', '[]', 0, '2020-12-12 11:09:58', '2020-12-12 11:09:58', '2021-12-12 14:39:58'),
('ac7ec726f6c81fc8dc938e52a66822510cd68dcc9b0733dde6be152604b932550c16a97e8308d158', 55, 1, 'Personal Access Token', '[]', 0, '2021-01-16 12:23:55', '2021-01-16 12:23:55', '2022-01-16 15:53:55'),
('ad8ddedeaf52b15a83d74cc18f192ccdd3f11a8113c1d6ab2be6e6e3beb8d692a786df2bcf9cbfbe', 13, 1, 'Personal Access Token', '[]', 0, '2020-12-12 05:04:02', '2020-12-12 05:04:02', '2021-12-12 08:34:02'),
('b7c94d4cd6863bfa8e3694ced26c5156dee5f2f0392f905592190d81f06c5bbf79d85960374ef6d3', 13, 1, 'Personal Access Token', '[]', 0, '2020-12-13 08:37:46', '2020-12-13 08:37:46', '2021-12-13 12:07:46'),
('ba752dd402c9f2ccaa58c383ecc9aacf027c9713c7470f20d2548e769cb20e3b0d1728ba2d2156bb', 13, 1, 'Personal Access Token', '[]', 0, '2020-12-12 10:57:04', '2020-12-12 10:57:04', '2021-12-12 14:27:04'),
('baf03a704b892ed9c01a7c8677fc110d298dca4cfe51c9165d8b869d64f54780c35913ef5fea9ae8', 55, 1, 'Personal Access Token', '[]', 0, '2020-12-27 10:37:00', '2020-12-27 10:37:00', '2021-12-27 14:07:00'),
('bbdb4c28621b2aa5b57d65e72581888fbc11bd6c0cfc5b4a157321212a17c3010390b029e9b410bc', 55, 1, 'Personal Access Token', '[]', 0, '2021-01-03 07:50:58', '2021-01-03 07:50:58', '2022-01-03 11:20:58'),
('bc452c7398416c4aa8088f6823502468596047b9c16ac24101c849d7956352ed064e1e6245e9b830', 55, 1, 'Personal Access Token', '[]', 0, '2021-01-18 05:55:18', '2021-01-18 05:55:18', '2022-01-18 09:25:18'),
('bd2df47e605f98ecfa386b58bdf43b0a0e694425ffe01eeb482bd19cc39d52385fb7075b498c20f9', 51, 1, 'NewToken', '[]', 0, '2020-12-16 10:14:51', '2020-12-16 10:14:51', '2021-12-16 13:44:51'),
('be5501247a32bb2a63c0f6c48fba029d238295e3391c1807d1cc6c54940924148e81b5ccc58258a6', 13, 1, 'Personal Access Token', '[]', 0, '2020-12-13 08:20:27', '2020-12-13 08:20:27', '2021-12-13 11:50:27'),
('bed5b06bf95359645cae06a26d0aab7a3aa93c6a6e04fe3738edad905d8619451bbfa70d2abf925d', 13, 1, 'Personal Access Token', '[]', 0, '2020-12-12 10:36:32', '2020-12-12 10:36:32', '2021-12-12 14:06:32'),
('bfc574089048031611dadde191c17927bbc65e17a84e28f1ee75d0bbe62065f90591aa7dad346b2e', 13, 1, 'Personal Access Token', '[]', 0, '2020-12-12 09:36:08', '2020-12-12 09:36:08', '2021-12-12 13:06:08'),
('c08bc8e0a2af012fe53dc633c14be4ba6ed4f3d7d7a1520767775d0bae9a069b543416d88c000aa0', 55, 1, 'Personal Access Token', '[]', 0, '2020-12-26 09:33:53', '2020-12-26 09:33:53', '2021-12-26 13:03:53'),
('c4d43330cd1702f84361a10f85831760a5a9685aaa8bc836fce1315d0786e695de7970ec9772cf87', 13, 1, 'Personal Access Token', '[]', 0, '2020-12-01 04:37:13', '2020-12-01 04:37:13', '2021-12-01 08:07:13'),
('c67ac7df6183b8ae423cfddd8850df1bde52bf3ae16d8e66b202a4ecb2c68b8953df99dc2ef76c39', 55, 1, 'Personal Access Token', '[]', 0, '2020-12-27 05:45:22', '2020-12-27 05:45:22', '2021-12-27 09:15:22'),
('c688fee4ece89197355f06faecdfae18362dd9195aee1c891af4c9bd17ce87cb2c12096db54c35a2', 13, 1, 'Personal Access Token', '[]', 0, '2020-12-12 05:28:38', '2020-12-12 05:28:38', '2021-12-12 08:58:38'),
('c9f1ab1af4b70be0a3f34b1184967201e768ece1232eb6ed5eaed5e13a0fb0fef75e046dad4775f7', 13, 1, 'Personal Access Token', '[]', 0, '2020-12-13 08:44:09', '2020-12-13 08:44:09', '2021-12-13 12:14:09'),
('ccc1c3d39651768813fed8b4f780c8a3552401465b07d2e57f537efb39ffd73e4c2c045bf9c73dfe', 13, 1, 'Personal Access Token', '[]', 0, '2020-12-01 05:37:23', '2020-12-01 05:37:23', '2021-12-01 09:07:23'),
('cec87fad9a4a202e10bf238647f79e1f0b635903cf6ba1bc23b1ba0264666ea8b2916f900823761f', 13, 1, 'Personal Access Token', '[]', 0, '2020-12-12 05:04:00', '2020-12-12 05:04:00', '2021-12-12 08:34:00'),
('d0b2087e9eed477b9875570fab6015db79495360aac05bb3b07918d1f62602d8d7825e43c833bf66', 13, 1, 'Personal Access Token', '[]', 0, '2020-12-10 05:51:43', '2020-12-10 05:51:43', '2021-12-10 09:21:43'),
('d0da29e86d126672e450bc1d2518d66f888a76ba7f93253654a29463d802b873af45f593ad367eb1', 55, 1, 'Personal Access Token', '[]', 0, '2021-01-18 06:08:43', '2021-01-18 06:08:43', '2022-01-18 09:38:43'),
('d3adde8e6a506a3b18101cfe63a203808120c0187023138204f99a01d47921ba1c7aa56c88b1603b', 53, 1, 'NewToken', '[]', 0, '2020-12-16 10:21:43', '2020-12-16 10:21:43', '2021-12-16 13:51:43'),
('d55c30daf12aa38bd2078587f8467048e362b61919430f7c6c8f99428688e596c50dca4f17b7f240', 55, 1, 'Personal Access Token', '[]', 0, '2020-12-26 06:24:18', '2020-12-26 06:24:18', '2021-12-26 09:54:18'),
('d9f2fb39ea1973f56828c795a0d5c262379ffc455f1cc9421740ec84cf1ec1c6ad541eb0f9c4172d', 55, 1, 'Personal Access Token', '[]', 0, '2020-12-27 05:50:44', '2020-12-27 05:50:44', '2021-12-27 09:20:44'),
('dab3b7e42ad67219157ec0165aa8153af8533c8505e0833cc565025b374d341dd4da32d5476fc60a', 1, 1, 'Personal Access Token', '[]', 0, '2020-11-30 06:22:50', '2020-11-30 06:22:50', '2021-11-30 09:52:50'),
('db4280230ec868bf9688a6fe77691677476b151a172fca6e9e4c158c37f38e199bc30655da09e7a2', 13, 1, 'Personal Access Token', '[]', 0, '2020-12-01 04:13:27', '2020-12-01 04:13:27', '2021-12-01 07:43:27'),
('db51013c18f0c00d9abb780f65d314619ab2862ca4bfa998cf47c7dd18f8633266352b7ce2aaa55f', 13, 1, 'Personal Access Token', '[]', 0, '2020-12-13 09:44:12', '2020-12-13 09:44:12', '2021-12-13 13:14:12'),
('dcdb39cb28787ef1adb8c3f2ef30cd48131a34cd5c516e8a943846adcfc709963119735f4f562323', 55, 1, 'Personal Access Token', '[]', 0, '2020-12-26 09:37:39', '2020-12-26 09:37:39', '2021-12-26 13:07:39'),
('dd2495c3a76ff5cb283bc8872040cc6a50f87f05157a721164e8e397a7c52b98d763f7cfb7de9138', 13, 1, 'Personal Access Token', '[]', 0, '2020-12-13 10:34:53', '2020-12-13 10:34:53', '2021-12-13 14:04:53'),
('de516e1b7b58136fffd7c983b283bf38dc4594c4a10aab9fe0427413ca147f94f81d54782d0b5602', 55, 1, 'Personal Access Token', '[]', 0, '2021-01-03 06:11:43', '2021-01-03 06:11:43', '2022-01-03 09:41:43'),
('df5d57db45bee212078f162b97887e314829c285bf3b39bd8af8daf0617834e13bb482b4d0cb69c9', 55, 1, 'Personal Access Token', '[]', 0, '2020-12-26 10:01:16', '2020-12-26 10:01:16', '2021-12-26 13:31:16'),
('e0000de2f8ea83650ada25204c6cc1a62b38d1c79b2f20b0b1127350e90b72a180c8fc6f9a04a995', 13, 1, 'Personal Access Token', '[]', 0, '2020-12-12 04:46:01', '2020-12-12 04:46:01', '2021-12-12 08:16:01'),
('e0f9a0964f20462d716c76c9afd25251c6bee53f37757015dabf3d56aa3762556494470cf39a5cc1', 55, 1, 'Personal Access Token', '[]', 0, '2020-12-26 09:42:56', '2020-12-26 09:42:56', '2021-12-26 13:12:56'),
('e106d34e433aa9c38dfa44b219760615030e50456c6ad3d9cf6a45c088fac34940b5f8afc75840c9', 55, 1, 'Personal Access Token', '[]', 0, '2021-01-16 05:43:42', '2021-01-16 05:43:42', '2022-01-16 09:13:42'),
('e4b7b8dd50a0ecd0941b8a8dd7a9f8e64afaa29b4a9aa434cfe3203fa34d7629bbcff254525f845a', 55, 1, 'Personal Access Token', '[]', 0, '2021-01-18 06:08:46', '2021-01-18 06:08:46', '2022-01-18 09:38:46'),
('e72fcb83a8a4329932f10445db32b9b0141310792532203931971afff60cb482f626195ce5002723', 13, 1, 'Personal Access Token', '[]', 0, '2020-12-13 08:40:09', '2020-12-13 08:40:09', '2021-12-13 12:10:09'),
('ea64affa3c3fc3d4529ad0f58d404c00e16097fd449e1dca769b1360d137b0b74b4da33bb2250801', 13, 1, 'Personal Access Token', '[]', 0, '2020-12-01 10:33:59', '2020-12-01 10:33:59', '2021-12-01 14:03:59'),
('ed6449483cad26ce2fba6cb8bfe35fc089c13807a5af9d312f26c91078c84c58db98cefff4d55662', 55, 1, 'Personal Access Token', '[]', 0, '2021-01-16 12:27:54', '2021-01-16 12:27:54', '2022-01-16 15:57:54'),
('ed94c0d140fbf7bdf70ffc378e0ad6f5a98248a566505e5d78c752c6a5e85647d5d6ace2ddb6d2a7', 55, 1, 'Personal Access Token', '[]', 0, '2021-01-18 06:08:43', '2021-01-18 06:08:43', '2022-01-18 09:38:43'),
('edf70d9c730e7fececbe93c6ed5e03e2c4860102db9fc740fa967faf10527254872175715e720ecc', 55, 1, 'Personal Access Token', '[]', 0, '2021-01-16 07:05:55', '2021-01-16 07:05:55', '2022-01-16 10:35:55'),
('f0b61af05b4c22855348a8b1de11a5eecd3a013c55704c0fa89720837ec3dc9c176432440aa123e0', 55, 1, 'Personal Access Token', '[]', 0, '2020-12-27 05:53:47', '2020-12-27 05:53:47', '2021-12-27 09:23:47'),
('f1319656795becf56ac944975852a1bb456e691237db981b5a159120f04e0e08f8488cbdf146be07', 13, 1, 'Personal Access Token', '[]', 0, '2020-12-12 09:36:30', '2020-12-12 09:36:30', '2021-12-12 13:06:30'),
('f55ef3ddbea7686740c36ec7a737114f0ec9ba92d18be56a9bc10c747d9b1f7e9df536930257c29c', 13, 1, 'Personal Access Token', '[]', 0, '2020-12-10 05:35:00', '2020-12-10 05:35:00', '2021-12-10 09:05:00'),
('f5b6b256d96f412f7d396c6c3d86b1ac23fa5fc309e784b8699c112b40cffcee3e09f156fc8a5d2d', 55, 1, 'Personal Access Token', '[]', 0, '2021-01-16 11:54:39', '2021-01-16 11:54:39', '2022-01-16 15:24:39'),
('f5fc76d52c9a4caa1eccf67038ba7581b32be2e6a553b6e2cbd1f1b03175714bf1537d692d23cfcf', 55, 1, 'Personal Access Token', '[]', 0, '2020-12-26 10:46:32', '2020-12-26 10:46:32', '2021-12-26 14:16:32'),
('fb2cc287de5f03111e76ef3d4f5fad6df71937b126b1ff1f8a06029b95e713d5c76738649c99aa9a', 13, 1, 'Personal Access Token', '[]', 0, '2020-12-01 10:28:10', '2020-12-01 10:28:10', '2021-12-01 13:58:10'),
('fdb08945e1336163a871d765fdddff7ab640b8ed7a3a1b95332beb20d20bbf2337f941e743111d3c', 13, 1, 'Personal Access Token', '[]', 0, '2020-12-13 08:40:22', '2020-12-13 08:40:22', '2021-12-13 12:10:22'),
('ff88dfdcebdedd2fd6d4937f96a11262cb0f28453e70a2df0db6ab468a57662c6659a28eda8cae7a', 13, 1, 'Personal Access Token', '[]', 0, '2020-11-30 09:46:42', '2020-11-30 09:46:42', '2021-11-30 13:16:42');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'sGXqGmRuPQfXmT9u96aepFfAoycQjt12qge1O26e', NULL, 'http://localhost', 1, 0, 0, '2020-11-21 09:35:33', '2020-11-21 09:35:33'),
(2, NULL, 'Laravel Password Grant Client', '6aeFQUFDYdlAAsNnsGiG17iet6oS6HJN6RgYCyLz', 'users', 'http://localhost', 0, 1, 0, '2020-11-21 09:35:33', '2020-11-21 09:35:33');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-11-21 09:35:33', '2020-11-21 09:35:33');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pictures`
--

CREATE TABLE `pictures` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nameUrl` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `plants`
--

CREATE TABLE `plants` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `planttype` bigint(20) UNSIGNED NOT NULL,
  `temp` double(8,2) UNSIGNED NOT NULL,
  `light` double(8,2) UNSIGNED NOT NULL,
  `humidity_soil` double(8,2) UNSIGNED NOT NULL,
  `humidity_air` double(8,2) UNSIGNED NOT NULL,
  `soil_id` bigint(20) UNSIGNED NOT NULL,
  `fertilizer_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `plants`
--

INSERT INTO `plants` (`id`, `name`, `photo`, `planttype`, `temp`, `light`, `humidity_soil`, `humidity_air`, `soil_id`, `fertilizer_id`, `created_at`, `updated_at`) VALUES
(3, 'گل رز', '1608455318.png', 1, 10.00, 10.00, 10.00, 10.00, 1, 1, NULL, NULL),
(66, 'afra1', '1608455318.png', 1, 10.00, 10.00, 10.00, 10.00, 1, 1, '2020-12-20 05:38:38', '2020-12-20 05:38:38'),
(67, 'afra1', '1608455624.png', 1, 10.00, 10.00, 10.00, 10.00, 1, 1, '2020-12-20 05:43:44', '2020-12-20 05:43:44'),
(68, 'afra1', '1608455646.png', 1, 10.00, 10.00, 10.00, 10.00, 1, 1, '2020-12-20 05:44:06', '2020-12-20 05:44:06'),
(69, 'afra1', '1608455717.png', 1, 10.00, 10.00, 10.00, 10.00, 1, 1, '2020-12-20 05:45:17', '2020-12-20 05:45:17'),
(70, 'afra1', '1608456312.png', 1, 10.00, 10.00, 10.00, 10.00, 1, 1, '2020-12-20 05:55:12', '2020-12-20 05:55:12'),
(71, 'afra1', '1608456579.png', 1, 10.00, 10.00, 10.00, 10.00, 1, 1, '2020-12-20 05:59:39', '2020-12-20 05:59:39');

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE `profiles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `firstname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`id`, `user_id`, `firstname`, `lastname`, `avatar`, `created_at`, `updated_at`) VALUES
(9, 55, 'abolfazl', 'rahmani', '1608365007.svg', '2020-12-19 03:42:03', '2020-12-19 03:42:03');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `phonenumber` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `phonenumber`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(55, '09107879978', '2021-01-18 09:25:01', '$2y$10$deNcxI1nlDCwrCr7VrsX0.NykUDLJzQqZ7cBciJL.FB6bKcFTeCze', NULL, '2020-12-19 03:42:03', '2021-01-18 09:25:01');

-- --------------------------------------------------------

--
-- Table structure for table `watermodes`
--

CREATE TABLE `watermodes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `watermodes`
--

INSERT INTO `watermodes` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'tanker', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `automodes`
--
ALTER TABLE `automodes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `builders`
--
ALTER TABLE `builders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `builders_plant_id_foreign` (`plant_id`);

--
-- Indexes for table `builder_user`
--
ALTER TABLE `builder_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cache`
--
ALTER TABLE `cache`
  ADD UNIQUE KEY `cache_key_unique` (`key`);

--
-- Indexes for table `categoryfertilizers`
--
ALTER TABLE `categoryfertilizers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categorysoils`
--
ALTER TABLE `categorysoils`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_plants`
--
ALTER TABLE `category_plants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comands`
--
ALTER TABLE `comands`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comands_builder_id_foreign` (`builder_id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `genders`
--
ALTER TABLE `genders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `logs_builder_id_foreign` (`builder_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `pictures`
--
ALTER TABLE `pictures`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `plants`
--
ALTER TABLE `plants`
  ADD PRIMARY KEY (`id`),
  ADD KEY `plants_fertilizer_id_foreign` (`fertilizer_id`),
  ADD KEY `plants_planttype_foreign` (`planttype`),
  ADD KEY `plants_soil_id_foreign` (`soil_id`);

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `profiles_user_id_foreign` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_phonenumber_unique` (`phonenumber`);

--
-- Indexes for table `watermodes`
--
ALTER TABLE `watermodes`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `automodes`
--
ALTER TABLE `automodes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `builders`
--
ALTER TABLE `builders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `builder_user`
--
ALTER TABLE `builder_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `categoryfertilizers`
--
ALTER TABLE `categoryfertilizers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `categorysoils`
--
ALTER TABLE `categorysoils`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `category_plants`
--
ALTER TABLE `category_plants`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `comands`
--
ALTER TABLE `comands`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `genders`
--
ALTER TABLE `genders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pictures`
--
ALTER TABLE `pictures`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `plants`
--
ALTER TABLE `plants`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT for table `profiles`
--
ALTER TABLE `profiles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT for table `watermodes`
--
ALTER TABLE `watermodes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `builders`
--
ALTER TABLE `builders`
  ADD CONSTRAINT `builders_plant_id_foreign` FOREIGN KEY (`plant_id`) REFERENCES `plants` (`id`);

--
-- Constraints for table `comands`
--
ALTER TABLE `comands`
  ADD CONSTRAINT `comands_builder_id_foreign` FOREIGN KEY (`builder_id`) REFERENCES `builders` (`id`);

--
-- Constraints for table `logs`
--
ALTER TABLE `logs`
  ADD CONSTRAINT `logs_builder_id_foreign` FOREIGN KEY (`builder_id`) REFERENCES `builders` (`id`);

--
-- Constraints for table `plants`
--
ALTER TABLE `plants`
  ADD CONSTRAINT `plants_fertilizer_id_foreign` FOREIGN KEY (`fertilizer_id`) REFERENCES `categoryfertilizers` (`id`),
  ADD CONSTRAINT `plants_planttype_foreign` FOREIGN KEY (`planttype`) REFERENCES `category_plants` (`id`),
  ADD CONSTRAINT `plants_soil_id_foreign` FOREIGN KEY (`soil_id`) REFERENCES `categorysoils` (`id`);

--
-- Constraints for table `profiles`
--
ALTER TABLE `profiles`
  ADD CONSTRAINT `profiles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
