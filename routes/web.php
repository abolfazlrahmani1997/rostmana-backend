<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\User;
Route::get('/', function () {
return view('Website.index.index');

});
Route::get('/about', function () {
    return view('Website.aboutUs.index');

});
Route::get('/Contactus',function (){
    return view('Website.contactus.index');
});
Route::get('/UpdateLog','BuilderController@UpdeateLog');
Route::group(['prefix'=>'dashbord'],function (){
Route::resource('/','DashbordController');
Route::resource('/User','UserController');
Route::POST('/userlist','UserController@datatables');
Route::get('/User/destroy/{id}','UserController@destroy');
Route::get('/Builder/destroy/{id}','BuilderController@destroy');
Route::resource('/Builder','BuilderController');
Route::resource('/log','LogController');
Route::resource('/Plant','PlantController');
Route::resource('/Soil','SoilController');
Route::resource('/fertillize','CategoryfertilizerController');
Route::resource('/Typeplant','CategoryPlantController');
Route::resource('/material','GenderController');
});
